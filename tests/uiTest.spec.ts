import { test, expect } from '@playwright/test';
import LandingPage from "../pages/MainPage";

test.beforeEach(async ({ page}) => {
  const landingPage= new LandingPage(page);
  await landingPage.visitPage();
  await landingPage.login()
});

test('Verify that the items are sorted by Name ( A-Z ).', async ({ page }) => {
  const landingPage= new LandingPage(page);
  await landingPage. verifyUserLoggedIn()
  await landingPage. checkSorting() 
});

test('Verify that the items are sorted by Name ( Z-A ).', async ({ page }) => {
  const landingPage= new LandingPage(page);
  await landingPage. verifyUserLoggedIn()
  await landingPage. checkReverseSorting() 
});


