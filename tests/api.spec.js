

const { test, expect } = require('@playwright/test');

const payLoad= {
   "userId": 200,
   "id": 101,
   "title": "delectus aut autem",
   "completed": false
 }
 test('api test', async ({ page, request }) => {
  // Step 1: Read Total Number of Posts and Store in a Variable
 const loginResponse = await request.get("https://jsonplaceholder.typicode.com/posts");
 expect(loginResponse.ok()).toBeTruthy();
 const responseJson = await loginResponse.json();
 const totalPosts = responseJson.length;


 // Step 2: Create a New Post and Store its ID
 const newPost = {
   userId: 1,
   title: 'New Post Title',
   body: 'New Post Body'
 };
 const createResponse = await request.post("https://jsonplaceholder.typicode.com/posts", {
   json: newPost
 });
 expect(createResponse.ok()).toBeTruthy();
 const createdPostId = createResponse.json().id;


 // Step 3: Get Only the Created Post by ID
 const createdPost = await request.get(`https://jsonplaceholder.typicode.com/posts/${createdPostId}`);
 expect(createdPost.ok()).toBeTruthy();
 const createdPostJson = await createdPost.json();


 // Step 4: Replace Some Field in the Created Post with PATCH
 const updatedPostData = {
   title: 'Updated Post Title'
 };
 const patchResponse = await request.patch(`https://jsonplaceholder.typicode.com/posts/${createdPostId}`, {
   json: updatedPostData
 });
 expect(patchResponse.ok()).toBeTruthy();


 const updatedPost = await request.get(`https://jsonplaceholder.typicode.com/posts/${createdPostId}`);
 expect(updatedPost.ok()).toBeTruthy();
 const updatedPostJson = await updatedPost.json();


 // Step 5: Delete the Created Post by ID
 const deleteResponse = await request.delete(`https://jsonplaceholder.typicode.com/posts/${createdPostId}`);
 expect(deleteResponse.ok()).toBeTruthy();


 // Step 6: Check the Number of Posts to Ensure Integrity
 const remainingPosts = await request.get("https://jsonplaceholder.typicode.com/posts");
 expect(remainingPosts.ok()).toBeTruthy();
 const finalTotalPosts = (await remainingPosts.json()).length;


 console.log('Total number of posts:', totalPosts);
 console.log('ID of the created post:', createdPostId);
 console.log('Details of the created post:', createdPostJson);
 console.log('Details of the updated post:', updatedPostJson);
 console.log('Remaining posts after deletion:', finalTotalPosts);








  });
 
