import { expect, Locator, Page } from "@playwright/test";

export default class LoginPage {
    readonly page: Page;
    readonly usename_text_box: Locator;
    readonly password_text_box: Locator;
    readonly login_button: Locator;
    readonly menu_button: Locator;
    readonly inventory_items: Locator;
    readonly sortorder_za: Locator;
    readonly sort_items: Locator;

    constructor(page: Page) {
        this.page = page;
        // Locators for various elements on the page
        this.usename_text_box = page.locator('input[id="user-name"]');
        this.password_text_box = page.locator('input[id="password"]');
        this.login_button = page.locator('[id="login-button"]');
        this.menu_button = page.locator('[id="react-burger-menu-btn"]');
        this.inventory_items = page.locator('.inventory_item_name');
        this.sortorder_za = page.locator('.product_sort_container');
        this.sort_items = page.locator('.select_container');
    }

    async enterName(name: string) {
        // Enters the username in the username text box
        await this.usename_text_box.fill(name);
    }

    async enterPassword(pwd: string) {
        // Enters the password in the password text box
        await this.password_text_box.fill(pwd);
    }

    async login(username: string = 'standard_user', password: string = 'secret_sauce') {
        // Logs in with provided username and password or defaults
        await this.usename_text_box.clear();
        await this.usename_text_box.first().fill(username);
        await this.password_text_box.first().clear();
        await this.password_text_box.first().fill(password);
        await this.login_button.click();
    }

    async verifyUserLoggedIn() {
        // Verifies if the user is logged in by checking the visibility of menu button
        await expect(this.menu_button).toBeVisible();
    }

    async checkSorting() {
        // Clicks on the sort button, sorts items in ascending order, and verifies the sorting
        await this.sort_items.click();
        await this.sortorder_za.waitFor();
        await this.sortorder_za.selectOption('az');
        await this.page.keyboard.press('Escape');
        const items = await this.inventory_items.allInnerTexts();
        const sortedItems = [...items].slice().sort((a, b) => a.localeCompare(b));
        expect(items).toEqual(sortedItems);
    }

    async checkReverseSorting() {
        // Clicks on the sort button, sorts items in descending order, and verifies the sorting
        await this.sort_items.click();
        await this.sortorder_za.waitFor();
        await this.sortorder_za.selectOption('za');
        await this.page.keyboard.press('Escape');
        const items = await this.inventory_items.allInnerTexts();
        const sortedItems = [...items].slice().sort((a, b) => b.localeCompare(a));
        expect(items).toEqual(sortedItems);
    }

    async visitPage() {
        // Navigates to the login page
        await this.page.goto("https://www.saucedemo.com/");
    }
}
